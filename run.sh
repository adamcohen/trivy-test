#!/bin/sh

set -e

# trivy --skip-files "rust-app/Cargo.lock,php-app/composer.lock,ruby-app/Gemfile.lock,python-app/Pipfile.lock,node-app/package-lock.json" --exit-code 0 --no-progress --format template --template "@/tmp/trivy-gitlab.tpl" -o gl-container-scanning-report.json $IMAGE
trivy --skip-files "rust-app/Cargo.lock,php-app/composer.lock,ruby-app/Gemfile.lock,python-app/Pipfile.lock,node-app/package-lock.json" --exit-code 0 --no-progress --format template --template "@/contrib/trivy-gitlab.tpl" -o gl-container-scanning-report.json $IMAGE