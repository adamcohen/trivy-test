#!/usr/bin/env ruby

require 'json'
require 'digest'

exit 1 unless File.exist?('./tmp.json')

before_report = File.read('./tmp.json')
parsed_report = JSON.parse(before_report)
parsed_report['vulnerabilities'].each do |vulnerability|
  vulnerability['message'] = 'temp' if vulnerability['message'] == ''
  image, os = vulnerability.dig('location', 'image').split(' ', 2)
  vulnerability['location']['operating_system'] = os[1..-2].delete(" \t\r\n")
  vulnerability['cve'] = "#{vulnerability.dig('location', 'operating_system')}:#{vulnerability.dig('location', 'dependency', 'package', 'name')}:#{vulnerability['cve']}"
  vulnerability['location']['image'] = image
  vulnerability['id'] = Digest::SHA1.hexdigest("#{vulnerability['cve']}:#{vulnerability['location']}")
end

puts JSON.dump(parsed_report)
